# Jitsi Stats Collector

Jitsi Stats COllector is a little software usefull for rework the datas in a **InfluxDB** database.  
When a jitsi instance is reload, the count of all metrics is reset at 0 value. This software collect the datas and create a new measurement for store datas.  
So the metrics count can't be missing after a jitsi restarting.  

## Operation

All process is doing into the new measurement and his fields.  

![InfluxQL_QC_jitsi_doc.png](doc/InfluxQL_QC_jitsi_doc.png)

1. **First Step:** Collect the values into original field
2. **Second Step:** Push values into new status field
3. **Third Step:** Process a diff between status and reference fields
4. **Fourth Step:** Update reference value from status value

## Installation

> The Jitsi Stats Collector building required golang in version equal or superior at 1.16.

Clone the repository :

* SSH

```bash
git clone ssh://git@ci.linagora.com:7999/nbecu/jitsi-stats-collector.git /opt/jitsi-stats-collector
```

* HTTPS

```bash
git clone https://ci.linagora.com/nbecu/jitsi-stats-collector.git /opt/jitsi-stats-collector
```

Move into repository :

```bash
cd /opt/jitsi-stats-collector
```

Then, compile the code :

```bash
GOOS=linux GOARCH=amd64 go build -o /usr/bin/jitsi-stats-collector .
```

A new binary named `jitsi-stats-collector` has been created into ***/usr/bin*** directory.

### Configuration

> The global configuration file must to be located to ***/etc/jitsi-stats-collector.toml***

The tool will loop on each node wich exports datas in database :

```toml
nodes = [
    "node-01",
    "node-02",
    "node-03"
    ]
```

The tool will loop on each fields that we want rework datas :

```toml
fields = [
    "total_conferences_created",
    "total_participants"
    ]
```

Complete exemple :

```toml
[options]
# Logging in file if is true
log = false
# Path of logs file
log_path="/var/log/jitsi-stats-collector.log"
# Interval in seconds for collect datas
refresh = 60

[infodb]
# InfluxDB endpoint
#host = "http://"
#host = "https://myhost.com"
host = "https://xx.xx.xx.xx"
# InfluxDB port
port = 8086
# Database Name
database = "db_name"
# Admin user of databases
user = "db_user"
# Password of admin user
password = "user_password"

[target]
# All jitsi instances where to collect datas
nodes = [
    "node-01",
    "node-02",
    "node-03"
    ]
# Original measurement from telegraf : https://grafana.com/grafana/dashboards/11969
origin = "jitsi_stats"
# Destination measurement
destination = "jitsi-stats-collector"
# All originals jitsi fields to rework values
fields = [
    "total_conferences_created",
    "total_participants"
    ]
```

### Daemonize

Use systemd for daemonize the tool. Create service file ***/lib/systemd/system/jitsi-stats-collector.service*** :

```ini
[Unit]
Description=Jitsi Stats datas reworker tool
After=network-online.target
Wants=network-online.target

[Service]
SuccessExitStatus=143
User=root
Group=root
PIDFile=/var/run/jitsi-stats-collector/jitsi-stats-collector.pid
ExecStartPre=/usr/bin/jitsi-stats-collector -timestamps
ExecStart=/usr/bin/jitsi-stats-collector -daemon
Restart=on-failure
RestartSec=3

[Install]
WantedBy=multi-user.target
```

### Options

| Options Availables | Description                                                                                                            |
|:------------------:|:-----------------------------------------------------------------------------------------------------------------------|
| -daemon            | Run jitsi-stats-collector like a daemon for rework datas in real time.                                                 |
| -days              | Provides the number of days required at initialization for the return in time. Must be used only with the INIT option. |
| -init              | Launch the initialization of the history in the database                                                               |
| -timestamps        | Push current séries of timestamps. Could be used for overide wrong timestamps  in the database                         |

## Grafana

Jitsi Stats Collector work with this kind of Grafana dashboards : [Jitsi Stats Collector Panel](doc/jitsi_stats_collector_panel.json)

![jscp](doc/dashboard_pic.png)
