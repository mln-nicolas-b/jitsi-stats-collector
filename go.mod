module gitlab.com/littlenightbird/jitsi-stats-collector

go 1.16

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/influxdata/influxdb-client-go/v2 v2.2.3
	github.com/sirupsen/logrus v1.8.1
)
