package main

import (
	"context"
	"fmt"
	"strconv"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
)

const (
	org string = ""
)

type InfluxDB interface {
	InitConnection(endpoint, credentials string)
	GetQuery(query string)
	PostQuery(measurement string)
	BuildQuery(bucket, measurement, field, node string)
	Close()
}

type Connection struct {
	Conn   influxdb2.Client
	Bucket string
}

func (c *Connection) InitConnection(endpoint, credentials, db string) {
	c.Conn = influxdb2.NewClient(endpoint, credentials)
	c.Bucket = fmt.Sprintf("%s/autogen", db)
}

func (c *Connection) PostQuery(measurement, host, field string, value int, ts time.Time) error {
	writeAPI := c.Conn.WriteAPIBlocking("", c.Bucket)
	// create point using full params constructor
	p := influxdb2.NewPoint(measurement,
		map[string]string{"host": host},
		map[string]interface{}{field: value}, ts)
	// Write data
	err := writeAPI.WritePoint(context.Background(), p)
	if err != nil {
		return err
	}
	return nil
}

func (c *Connection) GetQuery(query string) (string, int, time.Time, error) {
	var t time.Time
	// Get query client. Org name is not used
	queryAPI := c.Conn.QueryAPI(org)
	// Supply string in a form database/retention-policy as a bucket. Skip retention policy for the default one, use just a database name (without the slash character)
	result, err := queryAPI.Query(context.Background(), query)
	if err == nil {
		for result.Next() {
			// if result.TableChanged() {
			// 	fmt.Printf("\n[*** TABLE ***]:\n%s\n", result.TableMetadata().String())
			// }
			// fmt.Printf("\n[*** ROW ***]: %s\n", result.Record().String())
			// fmt.Printf("[*** VALUE ***]: %v\n---\n", result.Record().Value())
			// fmt.Printf("[*** VALUE ***]: %v\n---\n", result.Record().Time())
			t = result.Record().Time()
			layout := "2006-01-02 00:00:00"
			ts := t.Format(layout)
			t, err = time.Parse(layout, ts)
			l.CustomError(err)
			r := fmt.Sprintf("%v", result.Record().Value())
			ir, err := strconv.Atoi(r)
			if err != nil {
				return "", 0, t, err
			}
			return r, ir, t, nil
		}
		if result.Err() != nil {
			return "", 0, t, err
		}
	} else {
		return "", 0, t, nil
	}
	return "", 0, t, nil
}

func (c *Connection) Close() {
	// Close client
	c.Conn.Close()
}

func BuildQuery(bucket, measurement, field, node string) string {
	return fmt.Sprintf(`from(bucket: "%s")
	|> range(start: -48h)
	|> last()
	|> filter(fn: (r) =>
	  r._measurement == "%s" and
	  r._field == "%s" and
	  r.host == "%s"
	)
	|> yield()`, bucket, measurement, field, node)
}

func InitQuery(bucket, measurement, field, node string, start, end int) string {
	return fmt.Sprintf(`from(bucket: "%s")
	|> range(
		start: -%dd,
		stop: -%dd
	)
	|> last()
	|> filter(fn: (r) =>
	  r._measurement == "%s" and
	  r._field == "%s" and
	  r.host == "%s"
	)
	|> yield()`, bucket, start, end, measurement, field, node)
}
