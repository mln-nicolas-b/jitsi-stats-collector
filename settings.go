package main

import (
	"fmt"

	"github.com/BurntSushi/toml"
)

type Settings interface {
	NewSettings(configFile string)
}

type Environnement struct {
	Parameters   options `toml:"options"`
	InfoDB       infoDB  `toml:"infodb"`
	Target       target  `toml:"target"`
	ReworkInfoDB reworkInfoDB
}

type options struct {
	Refresh int    `toml:"refresh"`
	Log     bool   `toml:"log"`
	LogPath string `toml:"log_path"`
}

type infoDB struct {
	Host     string `toml:"host"`
	Port     int    `toml:"port"`
	Database string `toml:"database"`
	User     string `toml:"user"`
	Password string `toml:"password"`
}

type reworkInfoDB struct {
	Credential string
	Endpoint   string
}

type target struct {
	Nodes                  []string `toml:"nodes"`
	Fields                 []string `toml:"fields"`
	OriginMeasurement      string   `toml:"origin"`
	DestinationMeasurement string   `toml:"destination"`
}

func (e *Environnement) NewSettings(configFile string) error {
	_, err := toml.DecodeFile(configFile, e)
	if err != nil {
		return err
	}
	e.ReworkInfoDB.Credential = fmt.Sprintf("%s:%s", e.InfoDB.User, e.InfoDB.Password)
	e.ReworkInfoDB.Endpoint = fmt.Sprintf("%s:%d", e.InfoDB.Host, e.InfoDB.Port)
	return nil
}
